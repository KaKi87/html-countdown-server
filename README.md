# HTML countdown server

A simple HTML countdown generator as an HTTP server.

## Getting started

### Requirements

- NodeJS

### Installation

- `npm install html-countdown-server -g`

### Usage

#### HTTP server

- `html-countdown-server`

Eventually followed by a port number (default is `8080`), eg. : `html-countdown-server 1234`

#### HTTP client

Minimal URL :

- `http://localhost:PORT/?time=HH`

The `time` parameter must follow one of the following formats :

- `HH`
- `HH:MM`
- `HH:MM:SS`

An invalid or undefined `time` parameter will return a 400 (Bad Request) HTTP error.

Optional parameters :

| Parameter         | Default value | Description        | Type                                                                               |
| ----------------- | ------------- | -------------------| ---------------------------------------------------------------------------------- |
| `textColor`       | `#4C5193`     | Text color         | [CSS color](https://developer.mozilla.org/en-US/docs/Web/CSS/color_value) (string) |
| `backgroundColor` | `#00000000`   | Background color   | [CSS color](https://developer.mozilla.org/en-US/docs/Web/CSS/color_value) (string) |
| `fontFamily`      | `Poppins`     | Font family        | [Google Fonts](https://fonts.google.com/) font (string)                            |
| `fontWeight`      | `700`         | Font weight        | Positive integer (usually light = `300`, normal = `400`, bold = `700`, etc.)       |
| `fontSize`        | `77px`        | Font size          | [CSS length](https://developer.mozilla.org/en-US/docs/Web/CSS/length)              |
| `endText`         | `LIVE`        | Countdown end text | String                                                                             |

Examples :
- http://localhost:8080/?time=12
- http://localhost:8080/?time=12:13
- http://localhost:8080/?time=12:13:14
- http://localhost:8080/?time=12:13:14&textColor=#FFF&backgroundColor=#000
- [http://localhost:8080/?time=12:13:14&textColor=white&backgroundColor=black&fontFamily=Roboto&fontWeight=400&fontSize=50px&endText=The end](http://localhost:8080/?time=12:13:14&textColor=white&backgroundColor=black&fontFamily=Roboto&fontWeight=400&fontSize=50px&endText=The%20end)

Yes, CSS color values supports `#` wich are automatically replaced by `%23` and string values supports spaces which are automatically replaced with `%20`.

#### Embedding compatibility

This tool has been successfully tested in [`<iframe>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/iframe), in [OBS Studio](https://obsproject.com/) as *browser source* and should work in [Streamlabs OBS](https://streamlabs.com/) and [Electron](https://www.electronjs.org/).

## Changelog

- `1.0.0` (2020-08-31) • Initial release
- `1.0.1` (2020-09-01)
    - Change default `backgroundColor` to `#00000000`
    - Fix potential path issue
- `1.0.2` (2020-09-02) • Fix number `10` being transformed to `010`