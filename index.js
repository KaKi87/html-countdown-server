#!/usr/bin/env node

const [ _, __, port = 8080 ] = process.argv;

const
    http = require('http'),
    url = require('url'),
    fs = require('fs'),
    path = require('path');

http.createServer((req, res) => {
    if(req.url === '/favicon.ico'){
        res.writeHead(404);
        res.end();
        return;
    }
    const { query } = url.parse(req.url, true);
    // https://stackoverflow.com/a/43176525
    if(!/^([0-1]?\d|2[0-3])(?::([0-5]?\d))?(?::([0-5]?\d))?$/.test(query.time)){
        res.writeHead(400);
        res.end();
        return;
    }
    fs.readFile(path.join(__dirname, './index.html'), (err, data) => {
        if(err) res.writeHead(500);
        else {
            res.writeHead(200, {
                'Content-Type': 'text/html',
                'Content-Length': data.length
            });
            res.write(data);
        }
        res.end();
    })
}).listen(port, () => console.log(`http://localhost:${port}`));